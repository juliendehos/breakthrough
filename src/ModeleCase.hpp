
#ifndef MODELECASE_HPP_
#define MODELECASE_HPP_

/// \brief Position d'une case sur le Plateau.
///
/// Indiçage ligne-colonne.
struct ModeleCase {
	/// \brief Position de la case.
	int _i, _j;

	/// \brief Constructeur par défaut : case en (0,0).
	ModeleCase();

	/// \brief Constructeur avec initialisation.
	ModeleCase(int i, int j);
};

/// \brief Opérateur inférieur (pour les conteneurs associatifs STL).
bool operator<(const ModeleCase & refCase1, const ModeleCase & refCase2);

/// \brief Opérateur égalité (pour les algorithmes STL).
bool operator==(const ModeleCase & refCase1, const ModeleCase & refCase2);

#endif
