
#ifndef MODELEPLATEAU_HPP_
#define MODELEPLATEAU_HPP_

#include "ModeleCoup.hpp"

#include <vector>

/// \brief Le plateau de jeu contienr/gère l'ensemble des pions ainsi que les règles de jeu.
class ModelePlateau {
public:
	enum TypePion {PION_HAUT, PION_BAS, AUCUN_PION};

private:
	static const int _tailleI = 8;
	static const int _tailleJ = 8;
	std::vector<TypePion> _tabPions;
	std::vector<ModeleCoup> _coupsPossibles;
	TypePion _pionCourant, _pionAdverse;

public:
	/// \brief Constructeur par défaut.
	ModelePlateau();

	/// \brief Constructeur par copie (permet de dupliquer le plateau courant pour calculer une IA).
	ModelePlateau(const ModelePlateau & plateau);

	/// \brief Réinitialise le plateau (2 rangées de pions en haut, 2 rangées en bas).
	void reinitialiser();

	/// \brief met à jour les pions et passe au coup suivant.
	///
	/// \return true si le coup a été appliqué
	/// \return false si le coup a été ignoré (coup non valide)
	bool jouerCoup(const ModeleCoup & refCoup);

	/// \brief Cherche si quelqu'un a gagné (un pion a traversé ou un joueur n'a plus de pion.
	///
	/// \return le type de pion qui a gagné ou AUCUN_PION si la partie n'est pas terminée.
	ModelePlateau::TypePion chercherVainqueur() const;

	/// \brief Retourne les coups possibles pour le joueur courant.
	const std::vector<ModeleCoup> & getRefCoupsPossibles() const;

	/// \brief Retourne le pion a une case donnée du plateau.
	TypePion getPion(const ModeleCase & refCase) const;

	/// \brief Retourne le pion a une position donnée du plateau.
	TypePion getPion(int i, int j) const;

	/// \brief Retourne le pion qui doit jouer.
	TypePion getPionCourant() const;

	/// \brief Retourne le pion adverse (qui ne joue pas le coup courant).
	TypePion getPionAdverse() const;

	/// \brief Retourne le nombre de lignes du plateau.
	int getTailleI() const;

	/// \brief Retourne le nombre de colonnes du plateau.
	int getTailleJ() const;

private:
	void setPion(const ModeleCase & refCase, TypePion pion);
	void setPion(int i, int j, TypePion pion);

	// calcule les coups possibles et les met dans _coupsPossibles
	void calculerCoupsPossibles();
};

#endif
