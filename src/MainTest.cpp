
#define BOOST_TEST_MODULE TestsUnitairesBreakthrough
#include <boost/test/included/unit_test.hpp>

#include "Controleur_test.hpp"
#include "ModeleCase_test.hpp"
#include "ModeleCoup_test.hpp"
#include "ModeleJeu_test.hpp"
#include "ModeleJoueur_test.hpp"
#include "ModelePlateau_test.hpp"
#include "Vue_test.hpp"
#include "VueDessin_test.hpp"
#include "VueMenu_test.hpp"

