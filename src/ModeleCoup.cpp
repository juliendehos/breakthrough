
#include "ModeleCoup.hpp"

ModeleCoup::ModeleCoup() {
}

ModeleCoup::ModeleCoup(const ModeleCase & caseInitiale, const ModeleCase & caseFinale) :
	_caseInitiale(caseInitiale), _caseFinale(caseFinale) {
}

ModeleCoup::ModeleCoup(int i0, int j0, int i1, int j1) :
	_caseInitiale(i0, j0), _caseFinale(i1, j1) {
}

bool operator ==(const ModeleCoup& refCoup1, const ModeleCoup& refCoup2) {
	return refCoup1._caseInitiale._i == refCoup2._caseInitiale._i
			and refCoup1._caseInitiale._j == refCoup2._caseInitiale._j
			and refCoup1._caseFinale._i == refCoup2._caseFinale._i
			and refCoup1._caseFinale._j == refCoup2._caseFinale._j;
}

