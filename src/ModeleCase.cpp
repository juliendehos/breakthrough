
#include "ModeleCase.hpp"

ModeleCase::ModeleCase() :
	_i(0), _j(0) {
}

ModeleCase::ModeleCase(int i, int j) :
	_i(i), _j(j) {
}

bool operator <(const ModeleCase& refCase1, const ModeleCase& refCase2) {
	return refCase1._i < refCase2._i or (refCase1._i == refCase2._i and refCase1._j < refCase2._j);
}

bool operator ==(const ModeleCase& refCase1, const ModeleCase& refCase2) {
	return refCase1._i == refCase2._i and refCase1._j == refCase2._j;
}
