
#ifndef CONTROLEUR_HPP_
#define CONTROLEUR_HPP_

#include "ModeleJeu.hpp"
#include "Vue.hpp"

#include <gtkmm.h>

/// \brief Controleur : fait le lien entre la Vue et le Modèle.
///
/// Point d'entrée de l'application. Utilisation : construire un Controleur puis run().
/// Une fois lancé, le contrôleur peut gérer les événements de l'interface graphique et, en fonction, mettre à jour le jeu.
class Controleur {
	ModeleJeu _jeu;
	Vue _vue;

public:
	/// \brief Constructeur par défaut.
	Controleur(int argc, char ** argv);

	/// \brief Lance l'application.
	void run();

	/// \brief Commence un nouveau jeu.
	void nouveauJeu(Glib::ustring typeJoueurHaut, Glib::ustring typeJoueurBas);

	/// \brief Joue un coup et teste si le jeu est fini.
	void actualiserJeu(const ModeleCase& refCase);

	/// \brief Teste si une case initiale a été choisi pour le coup.
	bool isCaseInitialeOk() const;

	/// \brief Retourne la case initiale choisie.
	///
	/// \pre isCaseInitialeOk() retourne true
	const ModeleCase & getRefCaseInitiale() const;

	/// \brief Nombre de lignes du plateau.
	int getTailleI() const;

	/// \brief Nombre de colonnes du plateau.
	int getTailleJ() const;

	/// \brief Retourne le type de pion à une case donnée du plateau.
	ModelePlateau::TypePion getPion(int i, int j) const;

	/// \brief Retourne les coups possibles.
	const std::vector<ModeleCoup> & getRefCoupsPossibles() const;
};

#endif
