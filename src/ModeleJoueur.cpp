
#include "ModeleJoueur.hpp"

//////////////////////////////////////////////////////////////
// ModeleJoueur
//////////////////////////////////////////////////////////////

bool ModeleJoueur::isCoupOk() const {
	return _isCaseInitialeOk and _isCaseFinaleOk;
}

const ModeleCoup& ModeleJoueur::getRefCoup() const {
	return _coupAJouer;
}

bool ModeleJoueur::isCaseInitialeOk() const {
	return _isCaseInitialeOk;
}

ModeleJoueur::ModeleJoueur() {
}

const ModeleCase& ModeleJoueur::getRefCaseInitiale() const {
	return _coupAJouer._caseInitiale;
}

//////////////////////////////////////////////////////////////
// ModeleJoueurHumain
//////////////////////////////////////////////////////////////

void ModeleJoueurHumain::nouveauCoup(ModelePlateau copiePlateau) {
	_isCaseInitialeOk = false;
	_isCaseFinaleOk = false;
	_coupsPossibles = copiePlateau.getRefCoupsPossibles();
}

ModeleJoueurHumain::ModeleJoueurHumain() {
}

void ModeleJoueurHumain::choisirCase(const ModeleCase& refCase) {
	// si la case est une case initiale possible, on la retient comme case initiale
	for (auto coup : _coupsPossibles) {
		if (refCase == coup._caseInitiale) {
			_coupAJouer._caseInitiale = refCase;
			_isCaseInitialeOk = true;
			return;
		}
	}
	// si on a une case initiale et que la case est une case finale possible, on retient comme case finale
	if (_isCaseInitialeOk) {
		for (auto coup : _coupsPossibles) {
			if (refCase == coup._caseFinale) {
				_coupAJouer._caseFinale = refCase;
				_isCaseFinaleOk = true;
				return;
			}
		}
	}
	// sinon on ne retient aucune case
	else {
		_isCaseInitialeOk = false;
		_isCaseFinaleOk = false;
	}
}

//////////////////////////////////////////////////////////////
// ModeleJoueurIA
//////////////////////////////////////////////////////////////

ModeleJoueurIA::ModeleJoueurIA() {
	_isCaseFinaleOk = true;
	_isCaseInitialeOk = true;
}

void ModeleJoueurIA::choisirCase(const ModeleCase& refCase) {
}

//////////////////////////////////////////////////////////////
// ModeleJoueurIARandom
//////////////////////////////////////////////////////////////

void ModeleJoueurIARandom::nouveauCoup(ModelePlateau copiePlateau) {
	auto coupsPossibles = copiePlateau.getRefCoupsPossibles();
	if (not coupsPossibles.empty()) {
		// choisit un coup aléatoirement parmi tous les coups possibles
		int i = genererAleatoire(coupsPossibles.size());
		_coupAJouer = coupsPossibles[i];
	}
}

int ModeleJoueurIARandom::genererAleatoire(int N) {
	std::uniform_int_distribution<int> distribution(0, N-1);
	return distribution(_engine);
}

ModeleJoueurIARandom::ModeleJoueurIARandom() :
// initialise la graine du RNG de façon différente selon le temps et selon le joueur
//(pour éviter d'avoir la même graine d'une exécution à l'autre ou d'un joueur à l'autre)
_engine(time(0) + (long long)this) {
}

//////////////////////////////////////////////////////////////
// ModeleJoueurIARandomAffame
//////////////////////////////////////////////////////////////

void ModeleJoueurIARandomAffame::nouveauCoup(ModelePlateau copiePlateau) {
	auto coupsPossibles = copiePlateau.getRefCoupsPossibles();
	ModelePlateau::TypePion pionAdverse = copiePlateau.getPionAdverse();

	// trouve les captures possibles
	std::vector<ModeleCoup> capturesPossibles;
	for (auto coup : coupsPossibles) {
		if (copiePlateau.getPion(coup._caseFinale) == pionAdverse) {
			capturesPossibles.push_back(coup);
		}
	}

	// choisit une capture si possible
	if (not capturesPossibles.empty()) {
		int i = genererAleatoire(capturesPossibles.size());
		_coupAJouer = capturesPossibles[i];
	}
	else {
		// si aucune capture possible, mouvement aléatoire
		if (not coupsPossibles.empty()) {
			int i = genererAleatoire(coupsPossibles.size());
			_coupAJouer = coupsPossibles[i];
		}
	}
}

ModeleJoueurIARandomAffame::ModeleJoueurIARandomAffame() {
}
