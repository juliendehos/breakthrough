
#ifndef MODELEJEU_HPP_
#define MODELEJEU_HPP_

#include "ModeleJoueur.hpp"
#include "ModelePlateau.hpp"

#include <gtkmm.h>
#include <memory>

/// \brief Le jeu contient/gère le plateau et les deux joueurs.
class ModeleJeu {

	ModelePlateau _plateau;
	std::unique_ptr<ModeleJoueur> _uptrJoueurHaut, _uptrJoueurBas;

public:
	/// \brief Crée une nouvelle partie avec deux joueurs de types données (humain, IA).
	void nouveauJeu(Glib::ustring typeJoueurHaut, Glib::ustring typeJoueurBas);

	/// \brief Met le jeu à jour quand on clique sur une case (passer au coup suivant, tester la fin du jeu...).
	void choisirCase(const ModeleCase & refCase);

	/// \brief Teste si quelqu'un a gagné (PION_HAUT ou PION_BAS) ou si la partie n'est pas terminée (AUCUN_PION).
	ModelePlateau::TypePion chercherVainqueur() const;

	/// \brief Teste si une case initiale du coup a été choisie (permet de l'afficher).
	bool isCaseInitialeOk() const;

	/// \brief Retourne la case initiale choisie pour le coup.
	///
	/// \pre isCaseInitialeOk() retourne true
	const ModeleCase & getRefCaseInitiale() const;

	/// \brief Nombre de lignes du plateau.
	int getTailleI() const;

	/// \brief Nombre de colonnes du plateau.
	int getTailleJ() const;

	/// \brief Retourne le pion contenu à une position donnée dans le plateau.
	ModelePlateau::TypePion getPion(int i, int j) const;

	/// \brief Retourne tous les coups possibles (pour le joueur courant).
	const std::vector<ModeleCoup> & getRefCoupsPossibles() const;

private:
	// instancie un Joueur correspondant au type donnée
	// todo il faudrait plutot utiliser du std::string
	ModeleJoueur * construireJoueur(const Glib::ustring & type) const;

	ModeleJoueur* getPtrJoueurCourant() const;
};

#endif
