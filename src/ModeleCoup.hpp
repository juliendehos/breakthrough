
#ifndef MODELECOUP_HPP_
#define MODELECOUP_HPP_

#include "ModeleCase.hpp"

/// \brief Un coup est un déplacement d'une Case initiale à une Case finale.
struct ModeleCoup {
	/// \brief Case initiale et finale du coup.
	ModeleCase _caseInitiale, _caseFinale;

	/// \brief Constructeur par défaut.
	ModeleCoup();

	/// \brief Constructeur avec initialisation d'après les positions.
	ModeleCoup(int i0, int j0, int i1, int j1) ;

	/// \brief Constructeur avec initialisation d'après les cases.
	ModeleCoup(const ModeleCase & caseInitiale, const ModeleCase & caseFinale);
};

/// \brief Opérateur d'égalité entre deux coup.
bool operator==(const ModeleCoup & refCoup1, const ModeleCoup & refCoup2);

#endif
