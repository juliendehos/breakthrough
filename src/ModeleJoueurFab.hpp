#ifndef __FAB__
#define __FAB__

#include"ModeleJoueur.hpp"

class ModeleJoueurFab: public ModeleJoueurIA{
public:
        ModeleJoueurFab();
        virtual void nouveauCoup(ModelePlateau copiePlateau);
	int evaluerMonPlateau(ModelePlateau copiePlateau);
        int negaMax(ModelePlateau copiePlateau, int profondeur);
        ModeleCoup coupAvecNegaMax(ModelePlateau copiePlateau, int profondeur);
};

#endif
