
#ifndef VUEDESSIN_HPP_
#define VUEDESSIN_HPP_

#include <gtkmm.h>
#include <set>

class Controleur;

/// \brief Vue : zone de dessin de l'interface graphique.
///
/// Permet d'afficher le plateau de jeu.
class VueDessin : public Gtk::DrawingArea {

	Controleur & _refControleur;

	int _tailleI;
	int _tailleJ;
	int _largeur;
	int _hauteur;
	float _deltaI;
	float _deltaJ;
	bool _partieEnCours;

public:
	/// \brief Constructeur par défaut avec reference vers le controleur.
	/// Constructeur à utiliser par l'interface principale pour construire la zone de dessin.
	VueDessin(Controleur & refControleur);

	/// \brief Initialise la zone de dessin.
	/// Méthode à appeler avant de lancer l'interface graphique.
	void initialiser();

	/// \brief Actualise l'affichage de la zone de dessin.
	///
	void actualiser();

	/// \brief Active la gestion des actions de jeu de l'utilisateur (clic souris).
	/// Permet d'arrêter le jeu quand l'un des joueurs a gagné.
	void commencerPartie();

	/// \brief Désactive la gestion des actions de jeu de l'utilisateur (clic souris).
	/// Permet d'arrêter le jeu quand l'un des joueurs a gagné.
	void arreterPartie();

protected:
	/// \brief Gère l'affichage.
	bool on_expose_event(GdkEventExpose* ptrEvent);

	/// \brief Gère le clic souris.
	bool on_button_press_event(GdkEventButton* ptrEvent);

private:
	// affiche un pion (disque d'une couleur donnée)
	void afficherPion(Cairo::RefPtr<Cairo::Context> ptrContext, int i, int j, float r, float g, float b) const;

	// affiche le contour d'une case
	void afficherCase(Cairo::RefPtr<Cairo::Context> ptrContext, const ModeleCase & refCase, float r, float g, float b) const;

	// retourne l'ensemble des cases initiales possibles pour le coup courant
	std::set<ModeleCase> calculerCasesInitiales() const;

	// retourne l'ensemble des cases finales possibles pour le coup courant et une case initiale donnée
	// (si la case initiale n'est pas dans les coups possibles, retourne un ensemble vide)
	std::set<ModeleCase> calculerCasesFinales(const ModeleCase & refCaseInitiale) const;
};

#endif
