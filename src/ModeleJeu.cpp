
#include "ModeleJeu.hpp"
#include "ModeleJoueurFab.hpp"

#include <cassert>

int ModeleJeu::getTailleI() const {
	return _plateau.getTailleI();
}

int ModeleJeu::getTailleJ() const {
	return _plateau.getTailleJ();
}

ModelePlateau::TypePion ModeleJeu::getPion(int i, int j) const {
	return _plateau.getPion(i, j);
}

bool ModeleJeu::isCaseInitialeOk() const {
	return getPtrJoueurCourant()->isCaseInitialeOk();
}

const ModeleCase& ModeleJeu::getRefCaseInitiale() const {
	return getPtrJoueurCourant()->getRefCaseInitiale();
}

ModeleJoueur* ModeleJeu::getPtrJoueurCourant() const {
	if (_plateau.getPionCourant() == ModelePlateau::PION_HAUT)
		return _uptrJoueurHaut.get();
	if (_plateau.getPionCourant() == ModelePlateau::PION_BAS)
		return _uptrJoueurBas.get();
	return 0;
}

const std::vector<ModeleCoup>& ModeleJeu::getRefCoupsPossibles() const {
	return _plateau.getRefCoupsPossibles();
}

void ModeleJeu::nouveauJeu(Glib::ustring typeJoueurHaut, Glib::ustring typeJoueurBas) {
	_uptrJoueurHaut.reset(construireJoueur(typeJoueurHaut));
	_uptrJoueurBas.reset(construireJoueur(typeJoueurBas));
	_plateau.reinitialiser();
	getPtrJoueurCourant()->nouveauCoup(_plateau);
}

ModelePlateau::TypePion ModeleJeu::chercherVainqueur() const {
	return _plateau.chercherVainqueur();
}

void ModeleJeu::choisirCase(const ModeleCase & refCase) {
	// dire au joueur de choisir la case
	ModeleJoueur * ptrJoueurCourant = getPtrJoueurCourant();
	ptrJoueurCourant->choisirCase(refCase);

	// voir si on peut jouer le coup et passer au suivant
	if (ptrJoueurCourant->isCoupOk()) {
		_plateau.jouerCoup( ptrJoueurCourant->getRefCoup() );

		// attention le joueur courant a changé
		getPtrJoueurCourant()->nouveauCoup(_plateau);

	}
}

ModeleJoueur* ModeleJeu::construireJoueur(const Glib::ustring & type) const {
	if (type == "Humain") return new ModeleJoueurHumain;
	if (type == "IARandom") return new ModeleJoueurIARandom;
	if (type == "IARandomAffame") return new ModeleJoueurIARandomAffame;
	if (type == "Fab") return new ModeleJoueurFab;
	assert(false);
	return 0;
}
