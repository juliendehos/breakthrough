
#include "Controleur.hpp"
#include "Vue.hpp"
#include "VueMenu.hpp"

VueMenu::VueMenu(Controleur & refControleur, Vue & refVue) :
_refControleur(refControleur), _refVue(refVue) {

	// initialise les types de joueurs possibles
	// la création des menus est automatique
	// Attention : ne pas mettre d'espace ou d'accent dans le type
	// Voir Jeu::construireJoueur pour instancier réellement ces types
	_typesJoueurs.push_back("Humain");
	_typesJoueurs.push_back("IARandom");
	_typesJoueurs.push_back("IARandomAffame");
	_typesJoueurs.push_back("Fab");
}

Gtk::Widget & VueMenu::initialiser(Gtk::Window & refWindow) {

	_ptrActionGroup = Gtk::ActionGroup::create();

	// menu jeu
	_ptrActionGroup->add(Gtk::Action::create("MenuJeu", "Jeu"));
	_ptrActionGroup->add(Gtk::Action::create("MenuJeuJouer", "Nouveau"), sigc::mem_fun(*this, &VueMenu::onMenuNouveauJeu));
	_ptrActionGroup->add(Gtk::Action::create("MenuJeuQuitter", "Quitter"), sigc::mem_fun(_refVue, &Vue::quit));

	// menu joueur du haut
	initialiserMenuJoueur("MenuJoueurHaut", "Pions du haut", _ptrRadiosHaut);

	// menu joueur du bas
	initialiserMenuJoueur("MenuJoueurBas", "Pions du bas", _ptrRadiosBas);

	// menu aide
	_ptrActionGroup->add(Gtk::Action::create("MenuAide", "Aide"));
	_ptrActionGroup->add(Gtk::Action::create("MenuAideAide", "Aide..."), sigc::mem_fun(*this, &VueMenu::onMenuAide));

	_ptrUIManager = Gtk::UIManager::create();
	_ptrUIManager->insert_action_group(_ptrActionGroup);
	refWindow.add_accel_group(_ptrUIManager->get_accel_group());

	Glib::ustring uiInfo;
	uiInfo += "<ui>";
	uiInfo += "  <menubar name='MenuBar'>";
	uiInfo += "    <menu action='MenuJeu'>";
	uiInfo += "      <menuitem action='MenuJeuJouer'/>";
	uiInfo += "      <menuitem action='MenuJeuQuitter'/>";
	uiInfo += "    </menu>";

	ajouterInfo("MenuJoueurHaut", uiInfo);
	ajouterInfo("MenuJoueurBas", uiInfo);

	uiInfo += "    <menu action='MenuAide'>";
	uiInfo += "      <menuitem action='MenuAideAide'/>";
	uiInfo += "    </menu>";
	uiInfo += "  </menubar>";
	uiInfo += "</ui>";

	_ptrUIManager->add_ui_from_string(uiInfo);

	return *(_ptrUIManager->get_widget("/MenuBar"));
}

void VueMenu::onMenuNouveauJeu() {
	Glib::ustring typeJoueurHaut = getActiveLabel(_ptrRadiosHaut);
	Glib::ustring typeJoueurBas = getActiveLabel(_ptrRadiosBas);
	_refControleur.nouveauJeu(typeJoueurHaut, typeJoueurBas);
}

void VueMenu::initialiserMenuJoueur( Glib::ustring prefix, Glib::ustring nomMenu,
		std::vector< Glib::RefPtr<Gtk::RadioAction> > & refRadios) {

	// menu principal
	_ptrActionGroup->add(Gtk::Action::create(prefix, nomMenu));
	Gtk::RadioAction::Group group;
	// sous menu
	for (auto typeJoueur : _typesJoueurs) {
		Glib::ustring name = prefix + typeJoueur;
		Glib::ustring label = typeJoueur;
		Glib::RefPtr<Gtk::RadioAction> ptrRadio = Gtk::RadioAction::create(group, name, label);
		_ptrActionGroup->add(ptrRadio, sigc::mem_fun(*this, &VueMenu::onMenuNouveauJeu) );
		refRadios.push_back(ptrRadio);
	}
}

void VueMenu::ajouterInfo(Glib::ustring prefix, Glib::ustring& refInfo) const {

	// menu du joueur
	refInfo += "    <menu action='";
	refInfo += prefix;
	refInfo += "'>";
	// sous-menu pour chaque type de joueur possible
	for (auto typeJoueur : _typesJoueurs) {
		refInfo += "      <menuitem action='";
		refInfo += prefix;
		refInfo += typeJoueur;
		refInfo += "'/>";
	}
	refInfo += "    </menu>";
}

Glib::ustring VueMenu::getActiveLabel(std::vector<Glib::RefPtr<Gtk::RadioAction> >& ptrRadios) const {
	Glib::ustring label("Humain");
	for (auto ptrAction : ptrRadios) {
		if (ptrAction->get_active()) {
			label = ptrAction->get_label();
			break;
		}
	}
	return label;
}

void VueMenu::onMenuAide() {
	_refVue.afficherMessage("Breakthrough 2.0",
			"\nEn cas de problème, contactez Fabien Teytaud.\n\nPour faire un don, contactez Julien Dehos.");
}
