
#include "Controleur.hpp"
#include "Vue.hpp"

Vue::Vue(int argc, char ** argv, Controleur & refControleur) :
_refControleur(refControleur), _kit(argc, argv),
_dessin(refControleur), _menu(refControleur, *this) {

	_window.set_title("Breakthrough 2.0");
	_window.set_size_request(640, 480);
	_window.set_resizable(false);

	_window.add(_box);
	_box.pack_start(_menu.initialiser(_window), Gtk::PACK_SHRINK);
	_box.pack_start(_dessin);

	_window.show_all();
	_dessin.initialiser();
}

void Vue::run() {
	_kit.run(_window);
}

void Vue::actualiserVue() {
	_dessin.actualiser();
}

void Vue::afficherMessage(const std::string & refTitre, const std::string & refMessage) {
	Glib::ustring titre(refTitre.c_str());
	Glib::ustring message(refMessage.c_str());
	Gtk::MessageDialog dialogue(_window, message);
	dialogue.set_title(titre);
	dialogue.run();
}

void Vue::quit() {
	_kit.quit();
}

void Vue::commencerPartie() {
	_dessin.commencerPartie();
}

void Vue::arreterPartie(ModelePlateau::TypePion pionVainqueur) {
	if (pionVainqueur == ModelePlateau::PION_HAUT) {
		afficherMessage("Fin du jeu", " Les pions du haut gagnent !");
	}
	if (pionVainqueur == ModelePlateau::PION_BAS) {
		afficherMessage("Fin du jeu", " Les pions du bas gagnent !");
	}
	_dessin.arreterPartie();
}
