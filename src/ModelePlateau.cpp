
#include "ModelePlateau.hpp"

#include <algorithm>
#include <cassert>

ModelePlateau::ModelePlateau() {
	reinitialiser();
}

void ModelePlateau::reinitialiser() {
	_tabPions = std::vector<TypePion>(_tailleI*_tailleJ, AUCUN_PION);
	for (int j=0; j<_tailleJ; j++) {
		setPion(0, j, PION_HAUT);
		setPion(1, j, PION_HAUT);
		setPion(_tailleI-1, j, PION_BAS);
		setPion(_tailleI-2, j, PION_BAS);
	}
	_pionCourant = PION_BAS;
	_pionAdverse = PION_HAUT;
	calculerCoupsPossibles();
}
#include <iostream>

bool ModelePlateau::jouerCoup(const ModeleCoup& refCoup) {
	// verifie que le coup est possible
	auto iter = std::find(_coupsPossibles.begin(), _coupsPossibles.end(), refCoup);
	if (iter == _coupsPossibles.end()) return false;

	// met a jour les cases
	setPion(refCoup._caseInitiale._i, refCoup._caseInitiale._j, AUCUN_PION);
	setPion(refCoup._caseFinale._i, refCoup._caseFinale._j, _pionCourant);

	// passe au coup suivant
	std::swap(_pionCourant, _pionAdverse);
	calculerCoupsPossibles();

	return true;
}

ModelePlateau::TypePion ModelePlateau::chercherVainqueur() const {

	// cherche si un joueur a atteint l'autre bord
	for (int j=0; j<_tailleJ; j++) {
		if (getPion(0, j) == PION_BAS) return PION_BAS;
		if (getPion(_tailleI-1, j) == PION_HAUT) return PION_HAUT;
	}

	// cherche si le joueur du bas n'a plus de pion
	auto iterBas = find(_tabPions.begin(), _tabPions.end(), PION_BAS);
	if (iterBas == _tabPions.end()) return PION_HAUT;

	// cherche si le joueur du haut n'a plus de pion
	auto iterHaut = find(_tabPions.begin(), _tabPions.end(), PION_HAUT);
	if (iterHaut == _tabPions.end()) return PION_BAS;

	return AUCUN_PION;
}

const std::vector<ModeleCoup>& ModelePlateau::getRefCoupsPossibles() const {
	return _coupsPossibles;
}

ModelePlateau::TypePion ModelePlateau::getPion(int i, int j) const {
	assert(i>=0);
	assert(j>=0);
	assert(i<_tailleI);
	assert(j<_tailleJ);
	return _tabPions[i*_tailleJ + j];
}

int ModelePlateau::getTailleI() const {
	return _tailleI;
}

int ModelePlateau::getTailleJ() const {
	return _tailleJ;
}

ModelePlateau::ModelePlateau(const ModelePlateau& refPlateau) :
	_tabPions(refPlateau._tabPions), _coupsPossibles(refPlateau._coupsPossibles),
	_pionCourant(refPlateau._pionCourant), _pionAdverse(refPlateau._pionAdverse) {
}

void ModelePlateau::setPion(int i, int j, TypePion pion) {
	assert(i>=0);
	assert(j>=0);
	assert(i<_tailleI);
	assert(j<_tailleJ);
	_tabPions[i*_tailleJ + j] = pion;
}

ModelePlateau::TypePion ModelePlateau::getPionCourant() const {
	return _pionCourant;
}

ModelePlateau::TypePion ModelePlateau::getPion(const ModeleCase& refCase) const {
	return getPion(refCase._i, refCase._j);
}

ModelePlateau::TypePion ModelePlateau::getPionAdverse() const {
	return _pionAdverse;
}

void ModelePlateau::setPion(const ModeleCase& refCase, TypePion pion) {
	setPion(refCase._i, refCase._j, pion);
}

void ModelePlateau::calculerCoupsPossibles() {

	_coupsPossibles.clear();
	int inc = _pionCourant == PION_BAS ? -1 : 1;

	// parcourt tout le plateau
	for (int i0=0; i0<_tailleI; i0++) {
		for (int j0=0; j0<_tailleJ; j0++) {
			// retient les pions courants
			if (getPion(i0, j0) == _pionCourant) {
				int i1 = i0+inc;
				if (i1<_tailleI and i1>=0) {
					// case en face : possible si aucun pion
					if (getPion(i1, j0) == AUCUN_PION)
						_coupsPossibles.push_back(ModeleCoup(i0, j0, i1, j0));
					// diagonale gauche : possible si dans le plateau et pas déjà un pion courant
					if (j0>0 and getPion(i1, j0-1) != _pionCourant)
						_coupsPossibles.push_back(ModeleCoup(i0, j0, i1, j0-1));
					// diagonale droite : possible si dans le plateau et pas déjà un pion courant
					if (j0<_tailleJ-1 and getPion(i1, j0+1) != _pionCourant)
						_coupsPossibles.push_back(ModeleCoup(i0, j0, i1, j0+1));
				}
			}
		}
	}
}
