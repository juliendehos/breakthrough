#include"ModeleJoueurFab.hpp"
#include<iostream>

ModeleJoueurFab::ModeleJoueurFab() {
	_isCaseFinaleOk = true;
	_isCaseInitialeOk = true;

}

void ModeleJoueurFab::nouveauCoup(ModelePlateau copiePlateau) {
	auto coupsPossibles = copiePlateau.getRefCoupsPossibles();
	if (not coupsPossibles.empty()) {
		_coupAJouer = coupAvecNegaMax(copiePlateau, 4);
	}
}

int ModeleJoueurFab::evaluerMonPlateau(ModelePlateau copiePlateau) {
	if (copiePlateau.chercherVainqueur()==copiePlateau.getPionCourant())
		return 1000.;
	if (copiePlateau.chercherVainqueur()==copiePlateau.getPionAdverse())
		return -1000.;

	short som = 0;

	// on compte nos pions - les pions adverses
	for (int i=0; i<copiePlateau.getTailleI(); i++) {
		for (int j=0; j<copiePlateau.getTailleJ(); j++) {
			if (copiePlateau.getPion(i,j)==copiePlateau.getPionCourant())
				som+=10;
			else
				if (copiePlateau.getPion(i,j)==copiePlateau.getPionAdverse())
					som-=10;

		}
	}

	const int coefsPlateau[8][8] = {
			{ 5, 15, 15,  5, 15, 15,  5},
			{ 2,  3,  3,  3,  3,  3,  2},
			{ 4,  6,  6,  6,  6,  6,  4},
			{ 7, 10, 10, 10, 10, 10,  7},
			{11, 15, 15, 15, 15, 15, 11},
			{16, 21, 21, 21, 21, 21, 16},
			{20, 28, 28, 28, 28, 28, 20},
			{36, 36, 36, 36, 36, 36, 36}
	};

	int coefPion = ModelePlateau::PION_HAUT==copiePlateau.getPionCourant() ? 1 : -1;
	for (int i=0; i<8; i++) {
		for (int j=0; j<8; j++) {
			if (copiePlateau.getPion(i,j)==ModelePlateau::PION_HAUT)
				som += coefsPlateau[i][j] * coefPion;
			else
				if (copiePlateau.getPion(i,j)==ModelePlateau::PION_BAS)
					som -= coefsPlateau[7-i][j] * coefPion;
		}
	}

	return som;
}

int ModeleJoueurFab::negaMax(ModelePlateau copiePlateau, int profondeur) {
	if ((copiePlateau.chercherVainqueur()!=copiePlateau.AUCUN_PION) or (profondeur<=0))
		return evaluerMonPlateau(copiePlateau);
	int max = -100000.;
	int score;

	auto coupsPossibles = copiePlateau.getRefCoupsPossibles();

	for (auto coup : coupsPossibles) {
		ModelePlateau uneCopie = copiePlateau;
		uneCopie.jouerCoup(coup);
		score = -negaMax(uneCopie, profondeur-1);
		if (score > max)
			max = score;
	}

	return max;
}

ModeleCoup ModeleJoueurFab::coupAvecNegaMax(ModelePlateau copiePlateau, int profondeur) {
	ModeleCoup best;
	int score;
	int max = -100000.;

	auto coupsPossibles = copiePlateau.getRefCoupsPossibles();

	for (auto coup : coupsPossibles) {
		ModelePlateau uneCopie = copiePlateau;
		uneCopie.jouerCoup(coup);
		score = -negaMax(uneCopie, profondeur-1);
//		uneCopie.print();
//		std::cout<<"score ici : "<<score<<std::endl;
//		getchar();
		if (score > max) {
			max = score;
			best = coup;
		}
	}       

	std::cout<<"Meilleur score trouve pour l IA: "<<max<<std::endl;
	return best;

}
