
#include "Controleur.hpp"
#include "Vue.hpp"

Controleur::Controleur(int argc, char ** argv) :
_vue(argc, argv, *this) {
}

void Controleur::run() {
	_jeu.nouveauJeu("Humain", "Humain");
	_vue.commencerPartie();
	_vue.run();
}

void Controleur::actualiserJeu(const ModeleCase& refCase) {
	// choisir la case dans le jeu
	_jeu.choisirCase(refCase);

	// chercher un pion vainqueur
	ModelePlateau::TypePion pionVainqueur = _jeu.chercherVainqueur();
	if (pionVainqueur != ModelePlateau::AUCUN_PION) {
		_vue.arreterPartie(pionVainqueur);
	}
	_vue.actualiserVue();
}

void Controleur::nouveauJeu(Glib::ustring typeJoueurHaut, Glib::ustring typeJoueurBas) {
	_jeu.nouveauJeu(typeJoueurHaut, typeJoueurBas);
	_vue.commencerPartie();
	_vue.actualiserVue();
}

int Controleur::getTailleI() const {
	return _jeu.getTailleI();
}

int Controleur::getTailleJ() const {
	return _jeu.getTailleJ();
}

ModelePlateau::TypePion Controleur::getPion(int i, int j) const {
	return _jeu.getPion(i, j);
}

bool Controleur::isCaseInitialeOk() const {
	return _jeu.isCaseInitialeOk();
}

const ModeleCase& Controleur::getRefCaseInitiale() const {
	return _jeu.getRefCaseInitiale();
}

const std::vector<ModeleCoup>& Controleur::getRefCoupsPossibles() const {
	return _jeu.getRefCoupsPossibles();
}
