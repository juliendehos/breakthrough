
#include "ModeleCoup.hpp"

BOOST_AUTO_TEST_CASE( TestModeleCoup_1) {
	ModeleCoup c;
	BOOST_CHECK_EQUAL(c._caseInitiale._i, 0);
	BOOST_CHECK_EQUAL(c._caseInitiale._j, 0);
	BOOST_CHECK_EQUAL(c._caseFinale._i, 0);
	BOOST_CHECK_EQUAL(c._caseFinale._j, 0);
}

BOOST_AUTO_TEST_CASE( TestModeleCoup_2) {
	ModeleCoup c(1, 2, 3, 4);
	BOOST_CHECK_EQUAL(c._caseInitiale._i, 1);
	BOOST_CHECK_EQUAL(c._caseInitiale._j, 2);
	BOOST_CHECK_EQUAL(c._caseFinale._i, 3);
	BOOST_CHECK_EQUAL(c._caseFinale._j, 4);
}

BOOST_AUTO_TEST_CASE( TestModeleCoup_3) {
	ModeleCoup c(ModeleCase(4,3), ModeleCase(2,1));
	BOOST_CHECK_EQUAL(c._caseInitiale._i, 4);
	BOOST_CHECK_EQUAL(c._caseInitiale._j, 3);
	BOOST_CHECK_EQUAL(c._caseFinale._i, 2);
	BOOST_CHECK_EQUAL(c._caseFinale._j, 1);
}

BOOST_AUTO_TEST_CASE( TestModeleCoup_4) {
	ModeleCoup c1(1, 2, 3, 4);
	ModeleCoup c2(1, 2, 3, 4);
	BOOST_CHECK_EQUAL(c1==c2, true);
}

BOOST_AUTO_TEST_CASE( TestModeleCoup_5) {
	ModeleCoup c1(0, 2, 3, 4);
	ModeleCoup c2(1, 2, 3, 4);
	BOOST_CHECK_EQUAL(c1==c2, false);
}

BOOST_AUTO_TEST_CASE( TestModeleCoup_6) {
	ModeleCoup c1(1, 0, 3, 4);
	ModeleCoup c2(1, 2, 3, 4);
	BOOST_CHECK_EQUAL(c1==c2, false);
}

BOOST_AUTO_TEST_CASE( TestModeleCoup_7) {
	ModeleCoup c1(1, 2, 0, 4);
	ModeleCoup c2(1, 2, 3, 4);
	BOOST_CHECK_EQUAL(c1==c2, false);
}

BOOST_AUTO_TEST_CASE( TestModeleCoup_8) {
	ModeleCoup c1(1, 2, 3, 0);
	ModeleCoup c2(1, 2, 3, 4);
	BOOST_CHECK_EQUAL(c1==c2, false);
}
