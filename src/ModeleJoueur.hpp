
#ifndef MODELEJOUEUR_HPP_
#define MODELEJOUEUR_HPP_

#include "ModelePlateau.hpp"

#include <random>

/// \brief Un joueur permet de décider un coup a jouer en fonction du Plateau.
class ModeleJoueur {
protected:
	bool _isCaseInitialeOk;
	bool _isCaseFinaleOk;

	/// \brief Le coup calculé et qui sera ensuite appliqué.
	ModeleCoup _coupAJouer;

public:
	ModeleJoueur();

	/// \brief On commence un nouveau coup.
	virtual void nouveauCoup(ModelePlateau copiePlateau) = 0;

	/// \brief Une case a été choisie.
	virtual void choisirCase(const ModeleCase & refCase) = 0;

	/// \brief Retourne si le coup a été complètement calculé et peut être joué.
	bool isCoupOk() const;

	/// \brief Retourne le coup à jouer.
	///
	/// \pre isCoupOk retourne true
	const ModeleCoup & getRefCoup() const;

	/// \brief Retourne si la case initiale du coup a été complètement calculée.
	bool isCaseInitialeOk() const;

	/// \brief Retourne la case initiale choisie pour le coup à jouer.
	///
	/// \pre isCaseInitialeOk retourne true
	const ModeleCase & getRefCaseInitiale() const;
};

/// \brief Joueur humain : on prendre simplement en compte les cases cliquées.
class ModeleJoueurHumain : public ModeleJoueur {
	std::vector<ModeleCoup> _coupsPossibles;

public:
	ModeleJoueurHumain();

	/// \brief Nouveau coup : RAS.
	void nouveauCoup(ModelePlateau copiePlateau);

	/// \brief On détermine si la case est initiale, finale ou non valide et on met à jour.
	void choisirCase(const ModeleCase & refCase);
};

/// \brief Joueur IA : classe de base pour les IA (il faut juste dériver nouveauCoup() pour remplir _coupAJouer).
class ModeleJoueurIA : public ModeleJoueur {
public:
	ModeleJoueurIA();

	/// \brief Surdéfinir cette fonction pour implémenter l'IA (qui doit remplir _coupAJouer).
	virtual void nouveauCoup(ModelePlateau copiePlateau) = 0;

	/// \brief Case choisie : RAS.
	void choisirCase(const ModeleCase & refCase);
};

/// \brief Joueur qui joue un coup au hasard parmi les coups possibles.
class ModeleJoueurIARandom : public ModeleJoueurIA {
	// générateur pseudo-aléatoire type Mersenne-Twister
	std::mt19937 _engine;
public:
	ModeleJoueurIARandom();

	/// \brief Calcule l'IA.
	virtual void nouveauCoup(ModelePlateau copiePlateau);

protected:
	/// \brief Génère un nombre aléatoire dans [0, N] selon une distribution uniforme.
	int genererAleatoire(int N);
};

/// \brief Joueur qui joue un coup au hasard parmi les coups possibles en privilégiant les captures de pions adverses.
class ModeleJoueurIARandomAffame : public ModeleJoueurIARandom {
public:
	ModeleJoueurIARandomAffame();
	/// \brief Calcule l'IA.
	virtual void nouveauCoup(ModelePlateau copiePlateau);
};

#endif

