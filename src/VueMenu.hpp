
#ifndef VUEMENU_HPP_
#define VUEMENU_HPP_

#include <gtkmm.h>

class Controleur;
class Vue;

/// \brief Vue : barre de menu du jeu.
///
/// Permet de relancer le jeu et de régler les types de joueurs.
class VueMenu {

	Controleur & _refControleur;
	Vue & _refVue;

	// structures de donnees pour le menu
	Glib::RefPtr<Gtk::UIManager> _ptrUIManager;
	Glib::RefPtr<Gtk::ActionGroup> _ptrActionGroup;

	// types de joueurs possibles
	std::vector<Glib::ustring> _typesJoueurs;

	// choix du type de joueur pour les joueurs du haut et du bas
	std::vector< Glib::RefPtr<Gtk::RadioAction> > _ptrRadiosHaut;
	std::vector< Glib::RefPtr<Gtk::RadioAction> > _ptrRadiosBas;

public:
	/// \brief Constructeur par défaut avec reference vers le controleur.
	/// Constructeur à utiliser par l'interface principale pour construire menu.
	VueMenu(Controleur & refControleur, Vue & refVueGui);

	/// \brief Initialise le menu et l'ajoute dans une fenetre principale.
	/// Méthode à appeler pour construire l'interface graphique.
	Gtk::Widget & initialiser(Gtk::Window & refWindow);

	/// \brief Gestion du menu Aide (affiche un message).
	void onMenuAide();

	/// \brief Gestion des menus des joueurs (choisit le type de joueur voulu).
	void onMenuNouveauJeu();

private:
	// crée un menu pour pouvoir choisir un type de joueur
	void initialiserMenuJoueur(Glib::ustring prefix, Glib::ustring nomMenu,
			std::vector< Glib::RefPtr<Gtk::RadioAction> > & ptrRadios);

	// ajoute les infos des menus joueurs dans le ustring, pour le UI manager
	void ajouterInfo(Glib::ustring prefix, Glib::ustring & refInfo) const;

	// retourne le label du RadioAction sélectionné parmi un ensemble
	Glib::ustring getActiveLabel(std::vector< Glib::RefPtr<Gtk::RadioAction> > & ptrRadios) const;
};

#endif
