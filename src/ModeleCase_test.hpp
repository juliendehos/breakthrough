
#include "ModeleCase.hpp"

BOOST_AUTO_TEST_CASE( TestModeleCase_1) {
	ModeleCase c;
	BOOST_CHECK_EQUAL(c._i, 0);
	BOOST_CHECK_EQUAL(c._j, 0);
}

BOOST_AUTO_TEST_CASE( TestModeleCase_2) {
	ModeleCase c(1,2);
	BOOST_CHECK_EQUAL(c._i, 1);
	BOOST_CHECK_EQUAL(c._j, 2);
}

BOOST_AUTO_TEST_CASE( TestModeleCase_3) {
	ModeleCase c1(1,2);
	ModeleCase c2(3,4);
	BOOST_CHECK_EQUAL(c1<c2, true);
}

BOOST_AUTO_TEST_CASE( TestModeleCase_4) {
	ModeleCase c1(4,2);
	ModeleCase c2(3,4);
	BOOST_CHECK_EQUAL(c1<c2, false);
}

BOOST_AUTO_TEST_CASE( TestModeleCase_5) {
	ModeleCase c1(3,2);
	ModeleCase c2(3,4);
	BOOST_CHECK_EQUAL(c1<c2, true);
}

BOOST_AUTO_TEST_CASE( TestModeleCase_6) {
	ModeleCase c1(3,5);
	ModeleCase c2(3,4);
	BOOST_CHECK_EQUAL(c1<c2, false);
}

BOOST_AUTO_TEST_CASE( TestModeleCase_7) {
	ModeleCase c1(3,5);
	ModeleCase c2(3,4);
	BOOST_CHECK_EQUAL(c1==c2, false);
}

BOOST_AUTO_TEST_CASE( TestModeleCase_8) {
	ModeleCase c1(3,4);
	ModeleCase c2(3,4);
	BOOST_CHECK_EQUAL(c1==c2, true);
}

BOOST_AUTO_TEST_CASE( TestModeleCase_9) {
	ModeleCase c1(2,4);
	ModeleCase c2(3,4);
	BOOST_CHECK_EQUAL(c1==c2, false);
}

