
#ifndef VUE_HPP_
#define VUE_HPP_

#include "VueDessin.hpp"
#include "VueMenu.hpp"

/// \brief Vue : interface graphique du jeu.
///
/// Permet de construire et de lancer l'interface graphique principale du jeu.
/// Transmet les actions de jeu au controleur.
/// Utilise gtkmm-2.4.
class Vue {

	Controleur & _refControleur;

	Gtk::Main _kit;
	Gtk::Window _window;
	VueDessin _dessin;
	VueMenu _menu;
	Gtk::VBox _box;		// pour placer le menu et la zone de dessin

public:
	/// \brief Contructeur par défaut.
	/// Constructeur à utiliser pour construire l'interface graphique du jeu.
	Vue(int argc, char ** argv, Controleur & refControleur);

	/// \brief Lance l'interface graphique.
	///
	/// Les initialisations sont faites à la construction donc dès que l'interface est construite, on peut lancer le jeu
	/// en appelant cette méthode.
	void run();

	/// \brief Termine l'interface.
	void quit();

	/// \brief Rafraichit l'affichage.
	void actualiserVue();

	/// \brief Active la gestion des actions de jeu de l'utilisateur (clic souris).
	/// Permet d'arrêter le jeu quand l'un des joueurs a gagné.
	void commencerPartie();

	/// \brief Désactive la gestion des actions de jeu de l'utilisateur (clic souris).
	/// Permet d'arrêter le jeu quand l'un des joueurs a gagné.
	void arreterPartie(ModelePlateau::TypePion pionVainqueur);

	/// \brief Affiche un message dans une boite de dialogue bloquante.
	void afficherMessage(const std::string & refTitre, const std::string & refMessage) ;
};

#endif
