/// \mainpage  Implémentation du Jeu Breakthrough.
///
/// Présentation du jeu :
/// - jeu inventé par Dan Troyka
/// - plateau 8x8
/// - 2 joueurs : pions noirs (en haut), pions blancs (en bas)
/// - objectif : placer un de ses pions de l'autre côté
/// - jeu à tour de rôle (mouvement ou capture)
/// - mouvement : un de ses pions, une des trois cases (vides) devant le pion, dans la limite du plateau
/// - capture : sur un mouvement en diagonal
///
/// Fonctionnalités proposées dans l'implémentation :
/// - interface graphique avec respect des règles de jeu et affichage des mouvements possibles
/// - choix de types de joueur (humain, IA)
/// - action à la souris (pour les joueurs de type IA, il faut aussi cliquer pour dérouler les actions).
///

#include "Controleur.hpp"

int main(int argc, char ** argv) {
	Controleur breakthrough(argc, argv);
	breakthrough.run();
	return 0;
}
