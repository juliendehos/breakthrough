
#include "Controleur.hpp"
#include "VueDessin.hpp"


VueDessin::VueDessin(Controleur & refControleur) :
_refControleur(refControleur) {
	add_events(Gdk::BUTTON_PRESS_MASK);
}

bool VueDessin::on_expose_event(GdkEventExpose* ptrEvent) {
	// recupere la fenetre dans laquelle dessiner
	Glib::RefPtr<Gdk::Window> refWindow = get_window();
	if(refWindow) {
		// recupere le contexte de dessin
		Cairo::RefPtr<Cairo::Context> ptrContext = refWindow->create_cairo_context();

		// affiche le fond
		ptrContext->set_source_rgb(0.3, 0.6, 0.3);
		ptrContext->rectangle(0, 0, _largeur, _hauteur);
		ptrContext->fill();
		ptrContext->stroke();

		// affiche la grille du plateau
		ptrContext->set_line_width(1.0);
		ptrContext->set_source_rgb(0.0, 0.0, 0.0);
		for (float i=0; i<=_hauteur; i+=_deltaI) {
			ptrContext->move_to(0, i);
			ptrContext->line_to(_largeur, i);
		}
		for (float j=0; j<=_largeur; j+=_deltaJ) {
			ptrContext->move_to(j, 0);
			ptrContext->line_to(j, _hauteur);
		}
		ptrContext->stroke();

		// affiche les pions
		for (int i=0; i<_tailleI; i++) {
			for (int j=0; j<_tailleJ; j++) {
				if (_refControleur.getPion(i, j) == ModelePlateau::PION_HAUT)
					afficherPion(ptrContext, i, j, 0.0, 0.0, 0.0);
				if (_refControleur.getPion(i, j) == ModelePlateau::PION_BAS)
					afficherPion(ptrContext, i, j, 1.0, 1.0, 1.0);
			}
		}

		if (_partieEnCours) {
			// affiche les cases initiales possibles (sans doublon)
			auto casesInitiales = calculerCasesInitiales();
			for (auto caseInitiale : casesInitiales)
				afficherCase(ptrContext, caseInitiale, 1.0, 0.0, 0.0);

			// si une case initiale est choisie, affiche les cases finales possibles
			if (_refControleur.isCaseInitialeOk()) {
				auto caseInitiale = _refControleur.getRefCaseInitiale();
				afficherCase(ptrContext, caseInitiale, 1.0, 1.0, 1.0);

				// affiche les cases finales possibles
				auto casesFinales = calculerCasesFinales(caseInitiale);
				for (auto caseFinale : casesFinales)
					afficherCase(ptrContext, caseFinale, 0.0, 0.0, 1.0);
			}
		}
	}

	return true;
}

bool VueDessin::on_button_press_event(GdkEventButton* ptrEvent) {
	if (_partieEnCours) {
		// trouve la case choisie
		int i = ptrEvent->y / _deltaI;
		int j = ptrEvent->x / _deltaJ;

		// met à jour le jeu
		_refControleur.actualiserJeu(ModeleCase(i, j));
	}
	return true;
}

void VueDessin::initialiser() {
	// recupere la fenetre dans laquelle dessiner
	Glib::RefPtr<Gdk::Window> refWindow = get_window();
	if(refWindow) {
		// recupere quelques paramètres de la fenetre
		_largeur = refWindow->get_width();
		_hauteur = refWindow->get_height();
		_tailleI = _refControleur.getTailleI();
		_tailleJ = _refControleur.getTailleJ();
		_deltaI = _hauteur / (float)_tailleI;
		_deltaJ = _largeur / (float)_tailleJ;
	}
}

void VueDessin::afficherPion(Cairo::RefPtr<Cairo::Context> ptrContext,
		int i, int j, float r, float g, float b) const {

	ptrContext->set_line_width(2.0);
	ptrContext->set_source_rgb(r, g, b);
	float scalePion = 0.2 * std::min(_deltaI, _deltaJ);
	ptrContext->save();
	ptrContext->translate((j+0.5)*_deltaJ, (i+0.5)*_deltaI);
	ptrContext->scale(scalePion, scalePion);
	ptrContext->arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
	ptrContext->stroke();
	ptrContext->restore();
}

void VueDessin::afficherCase(Cairo::RefPtr<Cairo::Context> ptrContext,
		const ModeleCase & refCase, float r, float g, float b) const {

	ptrContext->set_line_width(2.0);
	ptrContext->set_source_rgb(r, g, b);
	ptrContext->rectangle(refCase._j*_deltaJ, refCase._i*_deltaI, _deltaJ, _deltaI);
	ptrContext->stroke();
}

void VueDessin::actualiser() {
	Glib::RefPtr<Gdk::Window> refWindow = get_window();
	if(refWindow) refWindow->invalidate(false);
}

void VueDessin::commencerPartie() {
	_partieEnCours = true;
}

void VueDessin::arreterPartie() {
	_partieEnCours = false;
}

std::set<ModeleCase> VueDessin::calculerCasesInitiales() const {
	const std::vector<ModeleCoup> & refCoupsPossibles = _refControleur.getRefCoupsPossibles();
	std::set< ModeleCase > casesInitiales;
	for (auto coup : refCoupsPossibles)
		casesInitiales.insert( coup._caseInitiale );
	return casesInitiales;
}

std::set<ModeleCase> VueDessin::calculerCasesFinales(const ModeleCase & refCaseInitiale) const {
	const std::vector<ModeleCoup> & refCoupsPossibles = _refControleur.getRefCoupsPossibles();
	std::set< ModeleCase > casesFinales;
	for (auto coup : refCoupsPossibles) {
		ModeleCase caseInitialeCourante(coup._caseInitiale);
		if (refCaseInitiale == caseInitialeCourante) {
			casesFinales.insert( coup._caseFinale );
		}
	}
	return casesFinales;
}
