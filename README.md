# Breakthrough 
[![Build status](https://gitlab.com/juliendehos/breakthrough/badges/master/build.svg)](https://gitlab.com/juliendehos/breakthrough/pipelines) 

Breakthrough board game with some basic IA players.

![](doc/screenshot.png)

## dependencies 

- boost 
- gtkmm-2.4

## build & run 

```
make 
./bin/MainProg.out
```

